#!/bin/bash

# BAGEL testsuite script
# Copyright 2018 Michael Banck <mbanck@debian.org>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Limit testsuite to 4 cores
export BAGEL_NUM_THREADS=4

if [ ! -d test -o ! -d src/testimpl ]; then
	echo "test directories not found!"
	exit 1
fi

if [ -n "$1" ]; then
	testfiles=$1
else
	testfiles='*'
fi

compare() {
	testtype=$1
	testname=$2
	reference=$3

	# check for named reference variable
	echo $reference | grep -q ^reference
	if [ "$?" == 0 ]; then
		testreferences=(`sed -n  /$reference\(\).*{/,/return.out/p  src/testimpl/*.cc | grep out.*= | sed -e s/.*=.// -e s/\;$//`)
		# for reference in "${testreferences[@]}"; do echo "reference: $reference"; done
	else 
		testreferences=
	fi

	case $testtype in
		mp2_energy)
			grep 'MP2 total energy' ${testname}.out | grep -q -- $testreference ;;
		nevpt2_energy)
			grep 'NEVPT2 total energy' ${testname}.out | grep -q -- $(echo $testreference | sed s/...$//) ;;
		asd_energy)
			grep -A1 'Adiabatic States' ${testname}.out | grep -q -- $(echo $testreference | sed s/..$//) ;;
		ras_energy|fci_energy|relfci_energy)
			if [ -n "$testreferences" ]; then
				for reference in "${testreferences[@]}"; do
					grep -B3 ci.vector ${testname}.out | grep -q -- $reference
					if [ $? != 0 ]; then
						FAILED=$((FAILED+1))
						echo "FAILED."
						tail -50 ${testname}.out
						return; 
					else
						echo -n "."
					fi
				done
				echo -n " "
			else
				grep -A1 'Adiabatic States' ${testname}.out | grep -q -- $(echo $testreference | sed s/..$//)
			fi ;;
		*)
			grep -B2 -i converged ${testname}.out | grep -q -- $testreference ;;

	esac
	if [ $? != 0 ]; then
		FAILED=$((FAILED+1))
		echo "FAILED."
		tail -50 ${testname}.out
	else
		echo "PASSED."
	fi
}
	
FAILED=0
SKIPPED=0

for testfile in src/testimpl/test_${testfiles}.cc;  do
	if [ "$testfile" = "src/testimpl/test_molden.cc" ]; then continue; fi
	#echo "testfile: $testfile"
	for test in `grep BOOST_CHECK $testfile	| egrep -v '^\/\/'	| \
			sed -e s/.*compare.// -e s/\(./\-/ -e s/.\)./\ / -e s/,.*// -e s/..\;\$// -e s/-/\ / -e s/\(\)$// | \
			awk '{print $1  "," $2 "," $3}'`; do
		testtype=$(cut -f1 -d, <<<$test)
		testname=$(cut -f2 -d, <<<$test)
		testreference=$(cut -f3 -d, <<<$test)
		echo -n "running test case '$testname'... "
		# Skip test if testfile cannot be found
		if [ ! -f test/${testname}.json ]; then
			SKIPPED=$((SKIPPED+1))
			echo "SKIPPED."
			continue
		fi
		# Skip tests that are not energy checks for now
		echo $testtype | grep -q energy
		if [ $? != 0 ]; then
			SKIPPED=$((SKIPPED+1))
			echo "SKIPPED."
			continue
		fi
		BAGEL test/${testname}.json > ${testname}.out
		compare $testtype $testname $testreference
	done
done

if [ $SKIPPED -gt 0 ]; then
	echo "$SKIPPED tests skipped"
fi

if [ $FAILED -gt 0 ]; then
	echo "$FAILED tests failed"
else
	echo "All tests passed"
fi

exit $FAILED
