Source: bagel
Section: science
Priority: optional
Maintainer: Debichem Team <debichem-devel@lists.alioth.debian.org>
Uploaders: Michael Banck <mbanck@debian.org>,
           Graham Inggs <ginggs@debian.org>
Build-Depends: debhelper-compat (= 12),
               libblas-dev,
               libboost-regex-dev,
               libboost-serialization-dev,
               libboost-test-dev,
               liblapack-dev,
               libmpich-dev,
               libxc-dev,
               python3-minimal
Standards-Version: 4.6.2
Homepage: http://www.nubakery.org/
Vcs-Browser: https://salsa.debian.org/debichem-team/bagel
Vcs-Git: https://salsa.debian.org/debichem-team/bagel.git

Package: bagel
Architecture: alpha amd64 arm64 ia64 kfreebsd-amd64 loong64 mips64el ppc64 ppc64el riscv64 s390x sparc64
Depends: ${misc:Depends}, ${shlibs:Depends}
Breaks: bagel-data
Replaces: bagel-data
Description: Computational Chemistry Package
 BAGEL (Brilliantly Advanced General Electronic-structure Library) is a
 computational chemistry package aimed at large-scale parallel
 computations.  It specializes on highgly accurate methods and includes
 density-fitting and relativistic effects for most of the methods it
 implements.
 .
 It can compute energies and gradients for the following methods:
  * Hartree-Fock (HF)
  * Density-Functional Theory (DFT)
  * Second-order Moeller-Plesset perturbation theory (MP2)
  * Complete active space SCF (CASSCF)
  * Complete active space second order perturbation theory (CASPT2)
  * Extended multistate CASPT2 (XMS-CASPT2)
 .
 Additionally, it can compute energies for the following methods:
  * Configuration-interaction singles (CIS)
  * Full configuration-interaction (FCI)
  * Multi-state internally contracted multireference configuration-interaction
    (ic-MRCI)
  * N-electron valence-state second order perturbation theory (NEVPT2)
  * Active-space decomposition (ASD) for dimers and for multiple sites via
    density matrix renormalization group (ASD-DMRG)
 .
 BAGEL is able to optimize stationary geometries and conical intersections and
 to compute vibrational frequencies.
 .
 BAGEL does not include a disk interface, so computations need to fit in
 memory.
